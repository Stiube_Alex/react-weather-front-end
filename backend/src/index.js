import express from "express";
import mongoose from "mongoose";
import { ApolloServer } from "apollo-server-express";
import { resolvers } from "./resolvers.js";
import { typeDefs } from "./typeDefs.js";
import MONGO_URI from "../env.js";

const server = async () => {
  const app = express();
  const server = new ApolloServer({
    typeDefs,
    resolvers,
  });

  await server.start();
  server.applyMiddleware({ app, bodyParserConfig: { limit: "10mb" } });

  const uri = MONGO_URI;
  await mongoose.connect(uri, { useNewUrlParser: true });

  app.get("/", (req, res) => res.send("works"));

  app.listen({ port: 4001 }, () => {
    console.log("connected");
  });
};

server();
