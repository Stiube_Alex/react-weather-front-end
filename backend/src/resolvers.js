import { Weather } from "./models/weather.js";
import { Review } from "./models/review.js";

export const resolvers = {
  Query: {
    helloWorld: () => "hello world",
    Weathers: async () => await Weather.find(),
    WeatherByCity: async (_, { city }) => await Weather.findOne({ city: city }),
    WeatherById: async (_, { id }) => await Weather.findOne({ _id: id }),
    ReviewPerSearch: async (_, { city }) => await Review.find({ city: city }),
  },
  Mutation: {
    createWeather: async (
      _,
      { time, city, country, actual, min, max, title, speed }
    ) => {
      await new Weather({
        time,
        city,
        country,
        actual,
        min,
        max,
        title,
        speed,
      }).save();

      return "Weather has been created";
    },
    updateWeather: async (
      _,
      { id, time, city, country, actual, min, max, title, speed }
    ) => {
      await Weather.findByIdAndUpdate(id, {
        city,
        country,
        actual,
        min,
        max,
        title,
        speed,
      });
      return "Weather has been updatet";
    },

    deleteWeather: async (_, { id }) => {
      await Weather.findByIdAndRemove(id);
      return "Weather has been deleted";
    },

    addReview: async (_, { city, user, stars, comment, time, photo }) => {
      await new Review({ city, user, stars, comment, time, photo }).save();
      return "Review has been added";
    },
  },
};
