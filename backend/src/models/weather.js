import mongoose from "mongoose";

export const Weather = mongoose.model("Weather", {
  time: String,
  city: String,
  country: String,
  actual: String,
  min: String,
  max: String,
  title: String,
  speed: String,
});
