import mongoose from "mongoose";

export const Review = mongoose.model("Reviews", {
  city: String,
  user: String,
  stars: String,
  comment: String,
  time: String,
  photo: String,
});
