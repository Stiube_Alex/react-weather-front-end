import { gql } from "apollo-server-express";

export const typeDefs = gql`
  type Query {
    helloWorld: String!
    Weathers: [Weather!]!
    WeatherByCity(city: String!): Weather!
    WeatherById(id: ID!): Weather!
    ReviewPerSearch(city: String!): [Review!]!
  }

  type Weather {
    id: ID
    time: String
    city: String!
    country: String
    actual: String
    min: String
    max: String
    title: String
    speed: String
  }

  type Review {
    id: ID
    city: String
    user: String
    stars: Int
    comment: String
    time: String
    photo: String
  }

  type Mutation {
    createWeather(
      time: String!
      city: String!
      country: String!
      actual: String!
      min: String!
      max: String!
      title: String!
      speed: String!
    ): String!
    updateWeather(
      id: ID!
      time: String
      city: String
      country: String
      actual: String
      min: String
      max: String
      title: String
      speed: String
    ): String!
    deleteWeather(id: ID!): String!
    addReview(
      city: String!
      user: String
      stars: String!
      comment: String
      time: String!
      photo: String!
    ): String!
  }
`;
