import React from "react";
import { Link } from "react-router-dom";

const Nav = () => {
  return (
    <div className="shadowDiv botSpacer">
      <Link className="routeLink navBtn" to="/">
        <h1 className="navTitle">Home</h1>
      </Link>
      <Link className="routeLink navBtn" to="/admin">
        <h1 className="navTitle">Admin</h1>
      </Link>
      <Link className="routeLink navBtn" to="about">
        <h1 className="navTitle">About</h1>
      </Link>
    </div>
  );
};

export default Nav;
