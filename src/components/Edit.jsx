import React from "react";
import { useSelector } from "react-redux";
import { useDispatch } from "react-redux";
import { getOneById, updateById, getAllWeather } from "../redux/actions";
import { Link } from "react-router-dom";

export default function Edit() {
  const dispatch = useDispatch();
  const url = window.location.href.split("/edit/")[window.location.href.split("/edit/").length - 1];

  let defaultCity = "";
  let defaultCountry = "";
  let defaultActual = "";
  let defaultMin = "";
  let defaultMax = "";
  let defaultTitle = "";
  let defaultSpeed = "";

  const load = () => {
    dispatch(getOneById(url));
  };

  const select = useSelector(data => data);
  let temp = select?.getOneByIdSlice?.data?.WeatherById;

  defaultCity = temp?.city;
  defaultCountry = temp?.country;
  defaultActual = temp?.actual;
  defaultMin = temp?.min;
  defaultMax = temp?.max;
  defaultTitle = temp?.title;
  defaultSpeed = temp?.speed;

  const city = React.createRef();
  const country = React.createRef();
  const actual = React.createRef();
  const min = React.createRef();
  const max = React.createRef();
  const title = React.createRef();
  const speed = React.createRef();

  const clear = () => {
    city.current.value = "";
    country.current.value = "";
    actual.current.value = "";
    min.current.value = "";
    max.current.value = "";
    title.current.value = "";
    speed.current.value = "";

    defaultCity = "";
    defaultCountry = "";
    defaultActual = "";
    defaultMin = "";
    defaultMax = "";
    defaultTitle = "";
    defaultSpeed = "";
  };

  const dataToSend = {};
  const confirm = () => {
    dataToSend.id = url;
    dataToSend.city = city.current.value.toLowerCase();
    dataToSend.country = country.current.value;
    dataToSend.actual = actual.current.value;
    dataToSend.min = min.current.value;
    dataToSend.max = max.current.value;
    dataToSend.title = title.current.value;
    dataToSend.speed = speed.current.value;
    dispatch(updateById(dataToSend));
    dispatch(getAllWeather());
  };

  // prettier-ignore
  return (
    <>
    <div className="botSpacer">
        <button onClick={load} className="adminBtn" id="borderLeft"><i className="fas fa-sync backArrr"></i></button>
        <Link to="/update" ><button className="adminBtn" onClick={clear}><i className="fas fa-angle-double-left backArrrr"></i></button></Link>    
        <Link to="/admin" className="linkFixInEditCurrent"> <button onClick={confirm} className="adminBtn"><i className="fas fa-check confirmIconFix"></i></button></Link>
        <button  onClick={clear} className="adminBtn"><i className="fas fa-times editIconPadding"></i></button>
    </div>
    <h1 className="showTitle">Create</h1>
    <div className="shadowDiv">
       <div className="showBox grid firstTable">
           <span>Time</span>
           <span>City</span>
           <span>Country</span>
           <span>Temp</span>
           <span>Min</span>
           <span>Max</span>
           <span>Summary</span>
           <span>Wind</span>
       </div>
       <div className="showBox grid secondTable">
           <span>Default</span>
           <span><input defaultValue={defaultCity} className="editInput" type="text" ref={city} placeholder="City"/></span>
           <span><input defaultValue={defaultCountry} className="editInput" type="text" ref={country} placeholder="Country"/></span>
           <span><input defaultValue={defaultActual} className="editInput" type="text" ref={actual} placeholder="Temp"/></span>
           <span><input defaultValue={defaultMin} className="editInput" type="text" ref={min} placeholder="Min"/></span>
           <span><input defaultValue={defaultMax} className="editInput" type="text" ref={max} placeholder="Max"/></span>
           <span><input defaultValue={defaultTitle} className="editInput" type="text" ref={title} placeholder="Title"/></span>
           <span><input defaultValue={defaultSpeed} className="editInput" type="text" ref={speed} placeholder="Speed"/></span>
       </div>
    </div>
  </>
);
}
