import React from "react";
import { useSelector } from "react-redux";
import { useDispatch } from "react-redux";
import { getAllWeather } from "../redux/actions";
import { Link } from "react-router-dom";

export default function Admin() {
  const dispatch = useDispatch();
  const load = () => {
    dispatch(getAllWeather());
  };
  const select = useSelector(data => data);
  let temp = select?.allWeatherSlice?.data;

  const loader = [];
  if (temp) {
    for (let index = 0; index < temp.length; index++) {
      loader.push(
        <div key={index} className="showBox grid secondTable" id="tableHover">
          <span>{temp[index].time}</span>
          <span>{temp[index].city}</span>
          <span>{temp[index].country}</span>
          <span>{temp[index].actual}</span>
          <span>{temp[index].min}</span>
          <span>{temp[index].max}</span>
          <span>{temp[index].title}</span>
          <span>{temp[index].speed}</span>
        </div>
      );
    }
  }
  loader.reverse();

  const initialTable = (
    <>
      <div className="showBox grid firstTable">
        <span>Time</span>
        <span>City</span>
        <span>Country</span>
        <span>Temp</span>
        <span>Min</span>
        <span>Max</span>
        <span>Summary</span>
        <span>Wind</span>
      </div>
    </>
  );

  const menu = (
    <div className="shadowDiv botSpacer">
      <div className="btnBox">
        <button className="adminBtn" id="borderLeft" onClick={load}>
          <i className="fas fa-download width19px"></i>
        </button>
        <Link to="/add">
          <button className="adminBtn">
            <i className="fas fa-plus width19px"></i>
          </button>
        </Link>
        <Link to="/update">
          <button className="adminBtn">
            <i className="fas fa-wrench width19px"></i>
          </button>
        </Link>
      </div>
    </div>
  );

  const final = (
    <>
      <div className="shadowDiv">
        {loader.length !== 0 ? initialTable : null}
        {loader}
      </div>
    </>
  );

  return (
    <>
      {menu}
      {loader.length !== 0 ? <h1 className="showTitle">Database</h1> : null}
      {loader.length !== 0 ? final : null}
    </>
  );
}
