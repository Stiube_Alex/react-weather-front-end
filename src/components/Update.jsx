import React from "react";
import { Link } from "react-router-dom";
import { useSelector } from "react-redux";
import { useDispatch } from "react-redux";
import { deleteRow } from "../redux/actions";
import { getAllWeather } from "../redux/actions";

export default function Update() {
  const dispatch = useDispatch();
  const select = useSelector(data => data);
  let temp = select?.allWeatherSlice?.data;

  const deleteBtn = event => {
    const id = event.target.dataset.id;
    dispatch(deleteRow(id));
    dispatch(getAllWeather());
  };

  const updateLoader = [];
  if (temp) {
    for (let index = 0; index < temp.length; index++) {
      let nowId = "/edit/" + temp[index].id;
      updateLoader.push(
        <div id="paddingFix" key={index} className="showBox grid secondTable">
          <span>
            <Link to={nowId}>
              <button className="actionBtn">
                <i data-id={temp[index].id} className="fas fa-pen-fancy width19px"></i>
              </button>
            </Link>
            <button onClick={deleteBtn} className="actionBtn updatePageDeleteBtn">
              <i data-id={temp[index].id} className="fas fa-trash-alt trashIcon"></i>
            </button>
          </span>
          <span>{temp[index].city}</span>
          <span>{temp[index].country}</span>
          <span>{temp[index].actual}</span>
          <span>{temp[index].min}</span>
          <span>{temp[index].max}</span>
          <span>{temp[index].title}</span>
          <span>{temp[index].speed}</span>
        </div>
      );
    }
  }

  updateLoader.reverse();

  const table = (
    <>
      <div className="shadowDiv">
        {updateLoader.length > 0 ? (
          <>
            <div className="showBox grid firstTable">
              <span>Action</span>
              <span>City</span>
              <span>Country</span>
              <span>Temp</span>
              <span>Min</span>
              <span>Max</span>
              <span>Summary</span>
              <span>Wind</span>
            </div>
          </>
        ) : null}

        {updateLoader}
      </div>
    </>
  );

  return (
    <>
      <div className="shadowDiv botSpacer">
        <Link to="/admin">
          <button className="adminBtn" id="borderLeft">
            <i className="fas fa-angle-double-left backArr"></i>
          </button>
        </Link>
      </div>

      {updateLoader.length > 0 ? <h1 className="showTitle">Update</h1> : null}
      {updateLoader.length > 0 ? table : null}
    </>
  );
}
