import React from "react";
import { Link } from "react-router-dom";
import { useDispatch } from "react-redux";
import { insertData, getAllWeather } from "../redux/actions";

export default function Add() {
  const dispatch = useDispatch();

  let city = React.createRef();
  let country = React.createRef();
  let temp = React.createRef();
  let min = React.createRef();
  let max = React.createRef();
  let summary = React.createRef();
  let wind = React.createRef();

  const dataToLoad = {};
  const confirm = () => {
    const time = String(new Date()).split(" ")[4].substr(0, 5);

    dataToLoad.time = time;
    dataToLoad.city = city.current.value.toLowerCase();
    dataToLoad.country = country.current.value;
    dataToLoad.temp = temp.current.value;
    dataToLoad.min = min.current.value;
    dataToLoad.max = max.current.value;
    dataToLoad.summary = summary.current.value;
    dataToLoad.wind = wind.current.value;

    dispatch(insertData(dataToLoad));
    dispatch(getAllWeather());

    city.current.value = "";
    country.current.value = "";
    temp.current.value = "";
    min.current.value = "";
    max.current.value = "";
    summary.current.value = "";
    wind.current.value = "";
  };

  // prettier-ignore
  return (
      <>
      <div className="botSpacer" id="fixMenu">
        <div className="shadowDiv">
          <Link to="/admin" ><button className="adminBtn" id="borderLeft"><i className="fas fa-angle-double-left backArr"></i></button></Link>    
          <Link onClick={confirm} to="/admin"><button className="adminBtn"><i  className="confirmBtn fas fa-check"></i></button></Link> 
        </div>
      </div>
      <h1 className="showTitle">Create</h1>
      <div className="shadowDiv">
         <div className="showBox grid firstTable">
           <span>Time</span>
           <span>City</span>
           <span>Country</span>
           <span>Temp</span>
           <span>Min</span>
           <span>Max</span>
           <span>Summary</span>
           <span>Wind</span>
         </div>
         <div className="showBox grid secondTable">
           <span>Now</span>
           <span id="fixAddTable"><input className="addInput" type="text" ref={city} placeholder="City"/></span>
           <span id="fixAddTable"><input className="addInput" type="text" ref={country} placeholder="Country"/></span>
           <span id="fixAddTable"><input className="addInput" type="text" ref={temp} placeholder="Temp"/></span>
           <span id="fixAddTable"><input className="addInput" type="text" ref={min} placeholder="Min"/></span>
           <span id="fixAddTable"><input className="addInput" type="text" ref={max} placeholder="Max"/></span>
           <span id="fixAddTable"><input className="addInput" type="text" ref={summary} placeholder="Summary"/></span>
           <span id="fixAddTable"><input className="addInput" type="text" ref={wind} placeholder="Wind"/></span>
         </div>
      </div>
    </>
  );
}
