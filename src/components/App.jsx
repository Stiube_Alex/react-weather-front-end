import Search from "./Search";
import Show from "./Show";
import Admin from "./Admin";
import About from "./About";
import Add from "./Add";
import Update from "./Update";
import Nav from "./Nav";
import Edit from "./Edit";
import Rating from "./Rating";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import "../design/css/style.css";

const Home = () => (
  <>
    <Search />
    <Show />
  </>
);
export default function App() {
  return (
    <>
      <div className="box">
        <Router>
          <Nav />
          <Switch>
            <Route path="/" exact component={Home} />
            <Route path="/about" exact component={About} />
            <Route path="/admin" exact component={Admin} />
            <Route path="/add" exact component={Add} />
            <Route path="/update" exact component={Update} />
            <Route path="/edit/:id" exact component={Edit} />
            <Route path="/Rating/:id" exact component={Rating} />
          </Switch>
        </Router>
      </div>
    </>
  );
}
