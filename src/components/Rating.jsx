import React from "react";
import { Route } from "react-router-dom";
import { useParams } from "react-router-dom";
import { useDispatch } from "react-redux";
import { addReview, reviewPerSearch } from "../redux/actions";
import Dropzone from "react-dropzone";

export default function Review() {
  let { id } = useParams();
  const dispatch = useDispatch();

  const divStars = React.createRef();

  const star1 = React.createRef();
  const star2 = React.createRef();
  const star3 = React.createRef();
  const star4 = React.createRef();
  const star5 = React.createRef();

  const userName = React.createRef();
  const comment = React.createRef();

  let rating = 0;

  const starArr = [star1, star2, star3, star4, star5];
  let clicked = false;
  let clickCount = 0;
  const colorize = e => {
    if (!clicked) {
      for (let index = 0; index < e.target.dataset.star; index++) {
        starArr[index].current.classList.add("starHover");
      }
    }
  };

  const unColorize = e => {
    if (!clicked) {
      for (let index = 0; index < 5; index++) {
        starArr[index].current.classList.remove("starHover");
      }
    }
  };

  const setStars = e => {
    rating = parseInt(e.target.dataset.star);
    clicked = true;
    if (clickCount % 2 !== 0) {
      for (let index = 0; index < e.target.dataset.star; index++) {
        starArr[index].current.classList.add("starHover");
      }
    } else {
      for (let index = 0; index < 5; index++) {
        starArr[index].current.classList.remove("starHover");
      }
      for (let index = 0; index < e.target.dataset.star; index++) {
        starArr[index].current.classList.add("starHover");
      }
    }
    clickCount++;
  };

  const clear = () => {
    userName.current.value = "";
    comment.current.value = "";
    divStars.current.childNodes.forEach(element => {
      element.classList.remove("starHover");
    });
    clicked = false;
  };

  const backBtn = (
    <Route
      render={({ history }) => (
        <button
          type="button"
          className="reviewBtn"
          onClick={() => {
            history.push("/");
            clear();
          }}
        >
          Back
        </button>
      )}
    />
  );

  const sendBtn = (
    <Route
      render={({ history }) => (
        <button
          type="button"
          className="reviewBtn"
          onClick={() => {
            if (rating > 0) {
              send();
              clear();
              history.push("/");
            }
          }}
        >
          Send
        </button>
      )}
    />
  );

  function getBase64(file) {
    return new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => resolve(reader.result);
      reader.onerror = error => reject(error);
    });
  }

  const inputFiles = React.createRef();

  const files = [];
  const submitImages = files_ => {
    for (let index = 0; index < files_.length; index++) {
      // console.log(files_);
      getBase64(files_[index]).then(data => files.push(data));
    }
  };

  const review = {};
  const send = () => {
    if (rating > 0) {
      review.city = id;
      review.user = userName.current.value;
      review.stars = rating;
      review.comment = comment.current.value;
      review.time = String(new Date()).split(" ")[4].substr(0, 5);
      setTimeout(() => {
        console.log(files);
        review.photo = files.join("__spliter__");
        dispatch(addReview(review));
      }, 500);
      dispatch(reviewPerSearch(id));
    }
  };

  const uploadForm = (
    <div className="uploadForm">
      <label htmlFor=""></label>
      <input ref={inputFiles} type="file" name="files" accept="iamge/*" multiple />
      <button onClick={submitImages}>Submit</button>
    </div>
  );

  const upload = (
    <Dropzone
      multiple
      onDrop={e => {
        submitImages(e);
      }}
    >
      {({ getRootProps, getInputProps }) => (
        <section className="dragNDropInput">
          <div {...getRootProps()}>
            <input {...getInputProps()} />
            <p>Upload images here!</p>
          </div>
        </section>
      )}
    </Dropzone>
  );

  // prettier-ignore
  return (
    <>
      <h1 className="showTitle botSpacer">Review</h1>
      <div className="shadowDiv">
        <div className="reviewForm">
          <input ref={userName} type="text" placeholder="Name" className="searchInput botSpacer" maxLength="10" required/>
          <div ref={divStars} className="starDiv botSpacer">
            <i data-star="1" ref={star1} onMouseEnter={colorize} onMouseLeave={unColorize} onClick={setStars} className="fas fa-star"></i>
            <i data-star="2" ref={star2} onMouseEnter={colorize} onMouseLeave={unColorize} onClick={setStars} className="fas fa-star"></i>
            <i data-star="3" ref={star3} onMouseEnter={colorize} onMouseLeave={unColorize} onClick={setStars} className="fas fa-star"></i>
            <i data-star="4" ref={star4} onMouseEnter={colorize} onMouseLeave={unColorize} onClick={setStars} className="fas fa-star"></i>
            <i data-star="5" ref={star5} onMouseEnter={colorize} onMouseLeave={unColorize} onClick={setStars} className="fas fa-star"></i>
          </div>
          <textarea ref={comment} placeholder="Write a review" className="searchInput botSpacer"></textarea>
          {/* {uploadForm} */}
          {upload}
          <div>
           {backBtn}
           <button onClick={clear} className="reviewBtn">Clear</button>
           {sendBtn}
          </div>
        {files}
        </div>
      </div>
    </>
  );
}
