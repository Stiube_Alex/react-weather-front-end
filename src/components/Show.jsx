import React from "react";
import { useSelector } from "react-redux";
import { Link } from "react-router-dom";

const Show = () => {
  const select = useSelector(data => data);
  const reviewDiv = React.createRef();
  const show = () => {
    reviewDiv.current.classList.remove("disapear", "slide-out-bottom");
  };
  const unShow = () => {
    reviewDiv.current.classList.add("slide-out-bottom");
    setTimeout(() => {
      reviewDiv.current.classList.add("disapear");
    }, 500);
  };
  // const {
  //   weatherSlice: { history },
  // } = select;
  // const {
  //   weatherSlice: { id, city, country, max, min, temp, time, speed, title },
  // } = select;

  let searchErr = false;
  let showWeather = ``;
  // let showHistory = [];
  if (!select?.fetchOneWeatherSlice?.data?.WeatherByCity) {
    searchErr = true;
  } else {
    searchErr = false;

    // prettier-ignore
    const { id, city, country, max, min, actual, time, speed, title } = select?.fetchOneWeatherSlice?.data?.WeatherByCity;
    // const { history } = select?.fetchOneWeatherSlice?.history;
    const name = city[0].toUpperCase() + city.slice(1);

    var route = `rating/${city}`;

    let avg;
    let average;
    let space = ``;
    if (select?.reviewPerSearchSlice) {
      const starsArr = select?.reviewPerSearchSlice[
        select?.reviewPerSearchSlice.length - 1
      ]?.ReviewPerSearch.map(e => e.stars);
      avg = starsArr ? starsArr.reduce((a, b) => a + b, 0) / starsArr.length : null;
    }
    if (avg !== null && isNaN(avg)) {
      average = null;
      space = ``;
    } else {
      if (avg !== null) {
        average = avg.toFixed(2);
        space = ` `;
      }
    }

    showWeather = (
      <>
        <div key={id} className="showBox grid9 secondTable">
          <span>{time}</span>
          <span>{name}</span>
          <span>{country}</span>
          <span>{actual}</span>
          <span>{min}</span>
          <span>{max}</span>
          <span>{title}</span>
          <span>{speed}</span>
          <div className="pointer" onClick={show}>
            <span id="review">
              <p>{average}</p>
              {space}
              {average ? <i className="fas fa-star red"></i> : <i className="fas fa-star fade"></i>}
            </span>
          </div>
        </div>
      </>
    );

    // showHistory = history.map(e => {
    //   return (
    //     <div className="showBox grid secondTable">
    //       <span>{e.time}</span>
    //       <span>{e.name}</span>
    //       <span>{e.country}</span>
    //       <span>{e.actual}</span>
    //       <span>{e.min}</span>
    //       <span>{e.max}</span>
    //       <span>{e.title}</span>
    //       <span>{e.speed}</span>
    //     </div>
    //   );
    // });
    // showHistory.reverse();
    // showHistory.shift();
  }

  // const historyPart = (
  //   <>
  //     <h1 className="showTitle firstTitleMarginFix">History</h1>
  //     <div className="shadowDiv">
  //       <div className="showBox grid firstTable">
  //         <span>Time</span>
  //         <span>City</span>
  //         <span>Country</span>
  //         <span>Temp</span>
  //         <span>Min</span>
  //         <span>Max</span>
  //         <span>Summary</span>
  //         <span>Wind</span>
  //       </div>
  //       {/* {showHistory} */}
  //     </div>
  //   </>
  // );

  let reviews = [];
  let l = select?.reviewPerSearchSlice.length;
  let indexTo = select?.reviewPerSearchSlice[l - 1]?.ReviewPerSearch.length;
  if (select?.reviewPerSearchSlice !== undefined) {
    for (let index = 0; index < indexTo; index++) {
      let data = select?.reviewPerSearchSlice[l - 1]?.ReviewPerSearch[index];
      reviews.push({
        time: data.time,
        city: data.city,
        user: data.user,
        stars: data.stars,
        comment: data.comment,
        photo: data.photo ? data.photo.trim().split("__spliter__") : [],
      });
    }
  }

  const htmlReviews = reviews.map(e => {
    const starArr = [];
    for (let index = 0; index < e.stars; index++) {
      starArr.push(<i className="fas fa-star red"></i>);
    }
    let photoArr = [];

    photoArr = e.photo.map(a => {
      return <img className="fixImageForReview" src={a} />;
    });

    return (
      <>
        <hr />
        <div>
          <div>
            <p className="upText">{e.user}</p>
            <p className="upText">{starArr}</p>
          </div>
          <p className="comment">{e.comment}</p>
          {photoArr.length > 0 ? <div className="fixImageForReviewDiv">{photoArr}</div> : null}
        </div>
      </>
    );
  });
  htmlReviews.reverse();
  const noneReviews = <h2>There are no reviews yet!</h2>;
  const weatherPart = (
    <>
      <h1 className="showTitle">Weather</h1>
      <div className="fixMargin">
        <div className="shadowDiv">
          <div className="showBox grid9 firstTable">
            <span>Time</span>
            <span>City</span>
            <span>Country</span>
            <span>Temp</span>
            <span>Min</span>
            <span>Max</span>
            <span>Summary</span>
            <span>Wind</span>
            <span>Rating</span>
          </div>
          {showWeather}
        </div>
        <div ref={reviewDiv} id="fixPosition" className="slide-top allRating shadowDiv disapear">
          <div>
            <div className="innerSize">
              <button onClick={unShow} id="xBtn" className="adminBtn">
                <i className="fas fa-times"></i>
              </button>
            </div>
            <Link to={route}>
              <button className="reviewBtn" id="fixSize">
                <div>
                  Add <i className="fas fa-star"></i>
                </div>
              </button>
            </Link>
            {htmlReviews.length > 0 ? htmlReviews : noneReviews}
          </div>
        </div>
      </div>
    </>
  );

  const err = (
    <>
      <h4 className="showTitle">Please enter a valid city!</h4>
    </>
  );

  return (
    <>
      {select?.fetchOneWeatherSlice?.data?.WeatherByCity ? weatherPart : null}
      {searchErr ? err : null}
      {/* {showHistory.length > 0 ? historyPart : null} */}
      <br />
    </>
  );
};

export default Show;
