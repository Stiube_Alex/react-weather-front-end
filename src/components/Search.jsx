import React from "react";
import { useDispatch } from "react-redux";
import { fetchOneWeather, reviewPerSearch } from "../redux/actions";

const Search = () => {
  const dispatch = useDispatch();

  const textInput = React.createRef();
  const gradeType = React.createRef();

  const handleClick = () => {
    if (textInput.current.value !== "") {
      dispatch(fetchOneWeather(textInput.current.value.toLowerCase()));
      dispatch(reviewPerSearch(textInput.current.value.toLowerCase()));
      textInput.current.value = "";
    }
  };

  const handleKeyPress = event => {
    if (event.key === "Enter") {
      handleClick();
    }
  };

  return (
    <div className="searchBox shadowDiv">
      <select name="gradeType" ref={gradeType} className="gradeType">
        <option value="metric">Celsius</option>
        <option value="imperial">Fahrenheit</option>
        <option value="kelvin">Kelvin</option>
      </select>
      <input
        required
        ref={textInput}
        onKeyPress={handleKeyPress}
        type="text"
        className="searchInput"
        placeholder="Enter a City"
      />
      <button onClick={handleClick} className="searchBtn">
        <i className="fas fa-search"></i>
      </button>
    </div>
  );
};

export default Search;
