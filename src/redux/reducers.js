import { combineReducers } from "redux";
import {
  // weatherSlice,
  allWeatherSlice,
  getOneByIdSlice,
  fetchOneWeatherSlice,
  reviewPerSearchSlice,
} from "./weather.reducer";

export default combineReducers({
  // weatherSlice,
  allWeatherSlice,
  getOneByIdSlice,
  fetchOneWeatherSlice,
  reviewPerSearchSlice,
});
