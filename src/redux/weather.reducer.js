import { createSlice } from "@reduxjs/toolkit";
import {
  /*fetchWeatherByCity,*/
  getAllWeather,
  getOneById,
  fetchOneWeather,
  reviewPerSearch,
} from "./actions";

// const initialState = { loading: "initial", city: {}, history: [] };
const allWeatherState = { loading: "initial" };
const oneState = { loading: "initial" };
const initState = { history: [] };
const reviews = [];

// export const weatherSlice = createSlice({
//   name: "weather",
//   initialState,
//   extraReducers: builder => {
//     builder.addCase(fetchWeatherByCity.fulfilled, (state, action) => {
//       const { city, unit } = action.meta.arg;
//       const { country } = action.payload.data.getCityByName;
//       const { title } = action.payload.data.getCityByName.weather.summary;
//       const { speed } = action.payload.data.getCityByName.weather.wind;
//       const { actual, min, max } = action.payload.data.getCityByName.weather.temperature;
//       const time = String(new Date()).split(" ")[4].substr(0, 5);

//       function unitConvertor(grade, speed) {
//         temp_ += grade;
//         min_ += grade;
//         max_ += grade;
//         speed_ += speed;
//       }

//       let time_ = time;
//       let city_ = city;
//       let country_ = country;
//       let title_ = title;
//       let temp_ = Math.round(actual * 10) / 10;
//       let min_ = Math.round(min * 10) / 10;
//       let max_ = Math.round(max * 10) / 10;
//       let speed_ = Math.round(speed * 10) / 10;

//       if (unit === "metric") {
//         unitConvertor("°C", " km/h");
//       } else if (unit === "kelvin") {
//         unitConvertor("°K", " km/h");
//       } else {
//         unitConvertor("°F", " m/h");
//       }

//       state.time = time_;
//       state.city = city_;
//       state.country = country_;
//       state.title = title_;
//       state.temp = temp_;
//       state.min = min_;
//       state.max = max_;
//       state.speed = speed_;

//       state.history.push({
//         time_,
//         city_,
//         country_,
//         title_,
//         temp_,
//         min_,
//         max_,
//         speed_,
//       });
//     });
//   },
// }).reducer;

export const allWeatherSlice = createSlice({
  name: "allWeather",
  initialState: allWeatherState,
  extraReducers: builder => {
    builder.addCase(getAllWeather.fulfilled, (state, action) => {
      state.data = action.payload.data.Weathers;
    });
  },
}).reducer;

export const getOneByIdSlice = createSlice({
  name: "getOne",
  initialState: oneState,
  extraReducers: builder => {
    builder.addCase(getOneById.fulfilled, (state, action) => {
      state.data = action.payload.data;
    });
  },
}).reducer;

export const fetchOneWeatherSlice = createSlice({
  name: "getOneByName",
  initialState: initState,
  extraReducers: builder => {
    builder.addCase(fetchOneWeather.fulfilled, (state, action) => {
      state.data = action.payload.data;
      // state.history.push(action.payload.data.WeatherByCity);
    });
  },
}).reducer;

export const reviewPerSearchSlice = createSlice({
  name: "getReviewsBySearchId",
  initialState: reviews,
  extraReducers: builder => {
    builder.addCase(reviewPerSearch.fulfilled, (state, action) => {
      state.push(action.payload.data);
    });
  },
}).reducer;
