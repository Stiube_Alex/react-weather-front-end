import { createAsyncThunk } from "@reduxjs/toolkit";
import { gql } from "graphql-request";

const fetchToUrl = "http://localhost:4001/graphql";

// export const fetchWeatherByCity = createAsyncThunk("extraction", async obj => {
//   const { city, unit } = obj;
//   const query = gql`
//       {
//          getCityByName(name:"${city}",config:{units:${unit}}) {
//             country
//             weather {
//               temperature {
//                 actual
//                 feelsLike
//                 min
//                 max
//               }
//               summary {
//               title
//         description
//               }
//               wind {
//                speed
//                 deg
//               }
//               clouds {
//                all
//                  visibility
//                 humidity
//               }
//            }
//           }
//        }

//    `;
//   const response = await fetch("https://graphql-weather-api.herokuapp.com/", {
//     method: "POST",
//     headers: {
//       "Content-Type": "application/json",
//       Accept: "application/json",
//     },
//     body: JSON.stringify({
//       query: query,
//     }),
//   });

//   const data = await response.json();
//   return data;
// });

export const getAllWeather = createAsyncThunk("getWeather", async () => {
  const query = gql`
    query Query {
      Weathers {
        id
        time
        city
        country
        actual
        min
        max
        title
        speed
      }
    }
  `;
  const response = await fetch(fetchToUrl, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Accept: "application/json",
    },
    body: JSON.stringify({ query: query }),
  });
  const data = await response.json();
  return data;
});

export const insertData = createAsyncThunk("insert", async dataToUpload => {
  const { time, city, country, temp, min, max, summary, wind } = dataToUpload;
  const query = gql`
    mutation {
      createWeather(
        time: "${time}"
        city: "${city}"
        country: "${country}"
        actual: "${temp}"
        min: "${min}"
        max: "${max}"
        title: "${summary}"
        speed: "${wind}"
      )
    }
  `;
  const response = await fetch(fetchToUrl, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Accept: "application/json",
    },
    body: JSON.stringify({ query: query }),
  });
  const data = await response.json();
  return data;
});

export const deleteRow = createAsyncThunk("delete", async idRow => {
  const query = gql`
    mutation {
      deleteWeather(id: "${idRow}")
    }
  `;
  const response = await fetch(fetchToUrl, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Accept: "application/json",
    },
    body: JSON.stringify({ query: query }),
  });
  const data = await response.json();
  return data;
});

export const getOneById = createAsyncThunk("getOne", async idRow => {
  const query = gql`
    query {
      WeatherById(id: "${idRow}") {
        id
        city
        country
        actual
        min
        max
        title
        speed
        time
      }
    }
  `;
  const response = await fetch(fetchToUrl, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Accept: "application/json",
    },
    body: JSON.stringify({ query: query }),
  });
  const data = await response.json();
  return data;
});

export const updateById = createAsyncThunk("update", async dataToUpdate => {
  const { id, city, country, actual, min, max, title, speed } = dataToUpdate;
  const query = gql`
    mutation {
      updateWeather(
        id: "${id}"
        city: "${city}"
        country: "${country}"
        actual: "${actual}"
        min: "${min}"
        max: "${max}"
        title: "${title}"
        speed: "${speed}"
      )
    }
  `;
  const response = await fetch(fetchToUrl, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Accept: "application/json",
    },
    body: JSON.stringify({ query: query }),
  });
  const data = await response.json();
  return data;
});

export const fetchOneWeather = createAsyncThunk("extraction", async name => {
  const query = gql`
   query {
    WeatherByCity(city: "${name}") {
        id
        city
        country
        actual
        min
        max
        title
        speed
        time
      }
    }`;
  const response = await fetch(fetchToUrl, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Accept: "application/json",
    },
    body: JSON.stringify({
      query: query,
    }),
  });
  const data = await response.json();
  return data;
});

export const reviewPerSearch = createAsyncThunk("extractReviews", async name => {
  const query = gql`
  query{
  ReviewPerSearch(city: "${name}") {
    city
    user
    stars
    time
    comment
    photo
  }
}
  `;
  const response = await fetch(fetchToUrl, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Accept: "application/json",
    },
    body: JSON.stringify({
      query: query,
    }),
  });
  const data = await response.json();
  return data;
});

export const addReview = createAsyncThunk("addReview", async reviewData => {
  const { city, user, stars, comment, time, photo } = reviewData;
  const query = gql`
    mutation {
      addReview(
        city: "${city}"
        user: "${user}"
        stars: "${stars}"
        comment: "${comment}"
        time: "${time}"
        photo: "${photo}"
      )
    }
  `;
  const response = await fetch(fetchToUrl, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Accept: "application/json",
    },
    body: JSON.stringify({
      query: query,
    }),
  });
  const data = await response.json();
  return data;
});
